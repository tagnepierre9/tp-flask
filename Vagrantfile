# -*- mode: ruby -*-
# vi: set ft=ruby :

# All Vagrant configuration is done below. The "2" in Vagrant.configure
# configures the configuration version (we support older styles for
# backwards compatibility). Please don't change it unless you know what
# you're doing.

$dockerInstallation = <<-SHELL
# Update the package database
sudo apt-get update

# Install required packages for Docker
sudo apt-get install -y apt-transport-https ca-certificates curl software-properties-common

# Add Docker’s official GPG key
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

# Set up the stable repository for Docker
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"

# Install Docker CE (Community Edition)
sudo apt-get update
sudo apt-get install -y docker-ce

# Add the vagrant user to the Docker group
sudo usermod -aG docker vagrant

# Ensure that the Docker socket has the correct permissions:
sudo chmod 666 /var/run/docker.sock
SHELL

$gitlabRunnerInstallation = <<-SHELL
# Add the official GitLab Runner repository
curl -L https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh | sudo bash

# Install GitLab Runner
sudo apt-get install -y gitlab-runner

# Add the GitLab Runner user to the Docker group
sudo usermod -aG docker gitlab-runner

# Restart Docker and GitLab Runner to apply changes
sudo systemctl restart docker
sudo systemctl restart gitlab-runner
SHELL

$ansibleInstallation = <<-SHELL
runuser -l vagrant -c 'echo -e "\n\n\n" | ssh-keygen -t rsa'
sudo apt-get update
sudo apt-get install -y sshpass
sudo apt-get install software-properties-common
sudo apt-get install -y ansible

# ssh key config
runuser -l vagrant -c 'cp ~/.ssh/id_rsa.pub /vagrant/'
echo -e "[localservers]\n192.168.33.12\n192.168.33.13" | sudo tee -a /etc/ansible/hosts
SHELL

Vagrant.configure("2") do |config|
  # The most common configuration options are documented and commented below.
  # For a complete reference, please see the online documentation at
  # https://docs.vagrantup.com.

  # Every Vagrant development environment requires a box. You can search for
  # boxes at https://vagrantcloud.com/search.
  config.vm.box = "hashicorp/bionic64"

  # Disable automatic box update checking. If you disable this, then
  # boxes will only be checked for updates when the user runs
  # `vagrant box outdated`. This is not recommended.
  # config.vm.box_check_update = false

  # Create a forwarded port mapping which allows access to a specific port
  # within the machine from a port on the host machine. In the example below,
  # accessing "localhost:8080" will access port 80 on the guest machine.
  # NOTE: This will enable public access to the opened port
  # config.vm.network "forwarded_port", guest: 80, host: 8080

  # Create a forwarded port mapping which allows access to a specific port
  # within the machine from a port on the host machine and only allow access
  # via 127.0.0.1 to disable public access
  # config.vm.network "forwarded_port", guest: 80, host: 8080, host_ip: "127.0.0.1"

  # Create a private network, which allows host-only access to the machine
  # using a specific IP.
  # config.vm.network "private_network", ip: "192.168.33.10"

  # Create a public network, which generally matched to bridged network.
  # Bridged networks make the machine appear as another physical device on
  # your network.
  # config.vm.network "public_network"

  # Share an additional folder to the guest VM. The first argument is
  # the path on the host to the actual folder. The second argument is
  # the path on the guest to mount the folder. And the optional third
  # argument is a set of non-required options.
  # config.vm.synced_folder "../data", "/vagrant_data"

  # Disable the default share of the current code directory. Doing this
  # provides improved isolation between the vagrant box and your host
  # by making sure your Vagrantfile isn't accessible to the vagrant box.
  # If you use this you may want to enable additional shared subfolders as
  # shown above.
  # config.vm.synced_folder ".", "/vagrant", disabled: true

  # Provider-specific configuration so you can fine-tune various
  # backing providers for Vagrant. These expose provider-specific options.
  # Example for VirtualBox:
  #
  # config.vm.provider "virtualbox" do |vb|
  #   # Display the VirtualBox GUI when booting the machine
  #   vb.gui = true
  #
  #   # Customize the amount of memory on the VM:
  #   vb.memory = "1024"
  # end
  #
  # View the documentation for the provider you are using for more
  # information on available options.

  # Enable provisioning with a shell script. Additional provisioners such as
  # Ansible, Chef, Docker, Puppet and Salt are also available. Please see the
  # documentation for more information about their specific syntax and use.
  # config.vm.provision "shell", inline: <<-SHELL
  #   apt-get update
  #   apt-get install -y apache2
  # SHELL

  config.vm.define "ansible" do |subconfig|
    subconfig.vm.box = "hashicorp/bionic64"
    subconfig.vm.hostname = "ansible"
    subconfig.vm.network "private_network", ip: "192.168.33.11"
    subconfig.vm.provision "shell", inline: $ansibleInstallation

    # Tests ssh: ssh 192.168.33.12
    # Tests ssh: ssh 192.168.33.13
    # Tests ansible: ansible localservers -m ping

    subconfig.vm.provision "shell", inline: $dockerInstallation
    subconfig.vm.provision "shell", inline: $gitlabRunnerInstallation
  end

  config.vm.define "server1" do |subconfig|
    subconfig.vm.box = "hashicorp/bionic64"
    subconfig.vm.hostname = "server1"
    subconfig.vm.network "private_network", ip: "192.168.33.12"
    subconfig.vm.provision "shell", inline: <<-SCRIPT
      runuser -l vagrant -c 'cat /vagrant/id_rsa.pub >> ~/.ssh/authorized_keys'
    SCRIPT
  end

  config.vm.define "server2" do |subconfig|
    subconfig.vm.box = "hashicorp/bionic64"
    subconfig.vm.hostname = "server2"
    subconfig.vm.network "private_network", ip: "192.168.33.13"
    subconfig.vm.provision "shell", inline: <<-SCRIPT
      runuser -l vagrant -c 'cat /vagrant/id_rsa.pub >> ~/.ssh/authorized_keys'
    SCRIPT
  end

end
