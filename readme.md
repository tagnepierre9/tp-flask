# README

Genere avec chatgpt

## Introduction

Ce projet contient une configuration Vagrant pour créer et gérer un environnement de développement comprenant plusieurs machines virtuelles, ainsi que les scripts nécessaires pour installer Docker, GitLab Runner, et Ansible. Il inclut également une application Python simple, avec une configuration Docker pour l'exécuter, et un pipeline CI/CD de base pour GitLab.

## Prérequis

- Vagrant
- VirtualBox
- Docker
- GitLab

## Configuration des machines virtuelles

Le `Vagrantfile` définit trois machines virtuelles:

1. **Ansible** - Cette machine est configurée avec Docker, GitLab Runner et Ansible.
2. **Server1** - Une machine serveur configurée pour être gérée par Ansible.
3. **Server2** - Une autre machine serveur configurée pour être gérée par Ansible.

## Installation

Pour configurer et démarrer les machines virtuelles, suivez les étapes ci-dessous:

1. Clonez le dépôt:

    ```sh
    git clone <URL_DU_DEPOT>
    cd <NOM_DU_DEPOT>
    ```

2. Démarrez les machines virtuelles avec Vagrant:

    ```sh
    vagrant up
    ```

## Détails de configuration

### Vagrantfile

Le `Vagrantfile` configure trois machines virtuelles:

- **Ansible**:
  - IP: `192.168.33.11`
  - Installe Docker, GitLab Runner et Ansible.
- **Server1**:
  - IP: `192.168.33.12`
  - Ajoute la clé publique SSH générée par Ansible.
- **Server2**:
  - IP: `192.168.33.13`
  - Ajoute la clé publique SSH générée par Ansible.

### Dockerfile

Le `Dockerfile` définit une image Docker pour une application Python:

- Utilise l'image `python:3.8-slim`.
- Installe les dépendances listées dans `requirements.txt`.
- Copie et exécute le fichier `app.py`.

### GitLab CI

Le fichier `.gitlab-ci.yml` définit un pipeline CI/CD avec les étapes suivantes:

- **Sanity Check**: Vérifie l'intégrité du code et des configurations.
- **Build**: Construit l'image Docker de l'application.
- **Test**: (Placeholder) Exécute les tests avec `pytest`.
- **Deploy**: Déploie l'application en utilisant Ansible.

### Scripts

- **sanity_check.sh**: Script basique de vérification.

  ```sh
  echo "All sanity checks passed."
  # more verifications
  ```

## Utilisation

### Accéder aux machines virtuelles

Pour accéder aux machines virtuelles, utilisez SSH:

- **Ansible**:

  ```sh
  ssh vagrant@192.168.33.11
  ```

- **Server1**:

  ```sh
  ssh vagrant@192.168.33.12
  ```

- **Server2**:

  ```sh
  ssh vagrant@192.168.33.13
  ```

### Exécuter l'application

Construisez l'image Docker et exécutez l'application:

```sh
docker build -t my-app:latest .
docker run -p 80:80 my-app:latest
```

### Pipeline CI/CD

Le pipeline CI/CD défini dans `.gitlab-ci.yml` exécute automatiquement les étapes de construction, de test et de déploiement à chaque push.
